import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {EventmasterComponent} from './master/eventmaster/eventmaster.component';
const routes: Routes = [
  {
    path: '',
    component: LoginComponent
},
{
  path: 'dashboard', component: DashboardComponent,
  children: [{
      path: 'master/event', component: EventmasterComponent
  }
  
  ]
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
