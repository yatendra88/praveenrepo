import { Component, OnDestroy ,OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import 'rxjs/add/operator/map'


class Person {
  id: number;
  firstName: string;
  lastName: string;
}

class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}

class event{
eventid: number;
eventcode: String;
eventname:String

}

@Component({
  selector: 'app-eventmaster',
  templateUrl: './eventmaster.component.html',
  styleUrls: ['./eventmaster.component.scss']
})
export class EventmasterComponent implements OnDestroy, OnInit {
  eventForm: FormGroup;
  @Output() closeModalEvent = new EventEmitter<boolean>();
 
  dtOptions: DataTables.Settings = {};
  persons: Person[] = [];
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();
  constructor(private http: HttpClient, private fb: FormBuilder) { }
  submitEvent(){
    const formValue = this.eventForm.value;
    alert(JSON.stringify(formValue));
  }
  resetForm(){
    this.eventForm.reset({
      eventcode: '',
      eventname: '',
      eventid: 0
      
  });

  }
  ngOnInit(): void {
    this.eventForm = this.fb.group({
      eventcode: ['', [
        Validators.required
              ]],
      eventname: ['', [
        Validators.required
              ]],
      eventid:0
    })
 

    const that = this;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {
        that.http
          .post<DataTablesResponse>(
            'https://angular-datatables-demo-server.herokuapp.com/',
            dataTablesParameters, {}
          ).subscribe(resp => {
            that.persons = resp.data;

            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },
      columns: [{ data: 'id' }, { data: 'firstName' }, { data: 'lastName' }]
    };
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  
  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }
}
